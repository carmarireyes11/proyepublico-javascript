const{ Router }  = require('express');
const router = Router();


const book = require('../models/book');


router.get ('/', async (req, res) => {

    const books = await book.find();
    res.json(books);

});

router.post ('/', async (req, res) => {
    const {title, author, isbn }= req.body;
    const imagePath = '/uploads/' + req.file.filename;
    const newBook = new book ({title, author, isbn, imagePath})
    await newBook.save();
    res.json({message:'book saved'});
    
});

router.delete('/:id', async (req, res) => {
    await book.findByIdAndDelete(req.params.id);
    res.json({message:'book deleted'});
    
})

module.exports = router;
